#include<stdio.h>
//①指针数组模拟二维数组
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 2,3,4,5,6 };
//	int arr3[] = { 3,4,5,6,7 };
//	int* parr[3] = { arr1,arr2,arr3 };
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", parr[i][j]);
//		}
//		printf("\n");
//	}
//
//	return 0;
//}
//②字符指针变量
// 把字符串的首字母h的地址存放到指针pc中
//int main()
//{
//	char ch = 'w';
//	char* pc = &ch;
//	*pc = 'a';
//	printf("%c\n", *pc);
//	return 0;
//}
//int main()
//{
//	const char* pc = "hello bit.";
//	printf("%s\n", pc);
//	return 0;
//}
//③字符串相关笔试题
//int main()
//{
//	char str1[] = "hello bit.";
//	char str2[] = "hello bit.";
//	const char* str3 = "hello bit.";
//	const char* str4 = "hello bit.";
//	if (str1 == str2)
//		printf("str1 and str2 are same\n");
//	else
//		printf("str1 and str2 sre not same\n");//对
//	if (str3 == str4)
//		printf("str3 and str sre same\n");//错
//	else
//		printf("str3 and str sre not same\n");
//	return 0;
//}
//这里str3和str4指向的是一个同一个常量字符串。c/c++会把常量字符串存到单独的一个内存区域，
//当几个指针指向同一个字符串时候，他们实际会指向同一块内存。但是用相同的常量字符串去初始化
// 不同的数组的时候就会开辟出不同的内存块
//④数组指针变量
//1. int *p1[10];    2.int (*p2)[10]
//2是数组指针
//解释：p先和*结合，说明p是个指针变量，然后指针指向的是一个大小为10个整型的数组
//⑤二维数组传参本质  形参可以写成数组也可写成指针
//#include <stdio.h>
//void test(int(*p)[5]),int r, int c)
//void test(int a[3][5], int r, int c)
//{
//	int i = 0;
//	int j = 0;
//		for (i = 0; i < r; i++)
//		{
//			for (j = 0; j < c; j++)
//			{
//				printf("%d ", a[i][j]);
//			}
//			printf("\n");
//		}
//}
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5}, {2,3,4,5,6},{3,4,5,6,7} };
//	test(arr, 3, 5);
//	return 0;
//}
//⑥函数指针变量
//#include <stdio.h>
//void test()
//{
//	printf("hehe\n");
//}
//int main()
//{
//	printf("test: %p\n", test);
//	printf("&test: %p\n", &test);
//	return 0;
//}
//结果：test: 005913CA
//    &test: 005913CA
//函数指针变量的创建
//void test()
//{
//	printf("hehe\n");
//}
//void (*pf1)() = &test;
//void (*pf2)() = test;
//int Add(int x, int y)
//{
//	return x + y;
//}
//int(*pf3)(int, int) = Add;
//int(*pf3)(int x, int y) = &Add;//x和y写上或者省略都是可以的
//函数指针变量的使用
#include <stdio.h>
int Add(int x, int y)
{
	return x + y;
}
int main()
{
	int(*pf3)(int, int) = Add;

	printf("%d\n", (*pf3)(2, 3));
	printf("%d\n", pf3(3, 5));
	return 0;
}
//输出结果：
//1  5
//2  8