#include<stdio.h>
//①数组名就是数组首元素的地址
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	printf("&arr[0]=%p\n", &arr[0]);
//	printf("arr=%p\n", arr);
//	return 0;
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* pa = &arr[0];
//	int* pb = &arr;
//	printf("%p\n", pa);
//	printf("%p\n", pb);
//	return 0;
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	printf("%p\n", &arr);
//	printf("%p\n", arr);
//	return 0;
//}
//
//②sizeof(数组名)计算整个数组的大小，单位字节
//③&数组名，这里的数组名表示整个数组，取出的是整个数组的地址
//④这里我们发现&arr[0]和&arr[0]+1相差四字节，arr和arr+1相差四字节，
//是因为&arr和&arr[0]都是首元素地址，+1就跳过一个元素。但是&arr表示1
//整个数组，+1跳过整个数组
//⑤使用指针访问数组
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%p\n",p+i);
//		//printf("%p\n",arr+i);  两种写法
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d\n", *(p + i));
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d\n", p[i]);    //p[i]和*(p + i)和*（arr+1）等价
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d\n", arr[i]);
//	}
//	return 0;
//}
//⑥一维数组传参本质
//void test(int arr[])
//{
//	int a2 = sizeof(arr) / sizeof(arr[0]);
//	printf("a2=%d\n", a2);                  a2=1
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int a1 = seizeof(arr) / sizeof(arr[0]);
//	printf("a1=%d\n", a1);                 a1=40
//	test(arr);
//	return 0;
//}
//本质上数组传参传递的是数组首元素的地址
//因此在函数内部sizeof（arr）计算的是一个地址的大小，而不是数组大小
//⑦冒泡排序   核心思想：两两相邻的元素进行比较

//void input(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		scanf_s("%d", arr + i); //给数组赋值
//	}
//}
//
//int count = 0;
//void bubble_sort(int *arr, int sz)
//{
//	//确定趟数
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int flag = 1;//假设已经满足顺序
//		//每一趟内部的比较
//		int j = 0;
//		for (j = 0; j < sz-1-i; j++)
//		{
//			count++;
//			if (arr[j] > arr[j + 1])
//			{
//				flag = 0;//还不是有序
//				//交换
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//		if (flag == 1)
//		{
//			break;
//		}
//	}
//}
//
//void print_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	//输入一些值
//	//1 2 3 4 5 6 7 8 9 10
//	//9 8 7 6 5 4 3 2 1 0
//	//2 1 9 7 0 3 4 8 5 6 
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	input(arr, sz);
//
//	//排序 - 写一个函数完成数组的排序，排成升序
//	bubble_sort(arr, sz);//使用冒泡排序
//	print_arr(arr, sz);
//	printf("\ncount = %d\n", count);
//	return 0;
//}
//二级指针
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	int** pa = &pa;
//	return 0;
//}